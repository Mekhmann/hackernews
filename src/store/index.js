import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    allNews: [],
    count: 0,
    ConvertDate: ''
  },
  mutations: {
    AddNews(state, payload) {
      // изменяем состояние
      state.allNews = payload;
      state.count ++;
    },
    setDate(state, payload){
      state.ConvertDate = payload
      //console.log(payload)
    }
  },
  actions: {},
  getters: {
    STATE_NEWS(state) {
      return state.allNews
    },
    STATE_DATE(state) {
      return state.ConvertDate
    }
  },
});