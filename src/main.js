import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueRouter from 'vue-router'
import { store } from './store/index'
import Vuetify from 'vuetify';
import MainPage from './components/MainPage'
import NewsPage from './components/NewsPage'

Vue.use(VueRouter)
Vue.use(Vuetify)
Vue.config.productionTip = false
Vue.config.silent = true;

const routes = [
  { 
    path: "",
    component: MainPage,
  },
  {
    path: "/:id",
    name: "item",
    component: NewsPage,
    props: true
  }
];

const router = new VueRouter({
  routes,
  mode: "history"
});

new Vue({
  vuetify,
  store,
  router,
  render: h => h(App)
}).$mount('#app')
